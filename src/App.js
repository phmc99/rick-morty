import { useEffect, useState } from "react";

import "./style.css";

import { Button } from "./components/Button";
import { Characters } from "./components/Characters";

function App() {
  const [characterList, setCharacterList] = useState([]);
  const [page, setPage] = useState(
    `https://rickandmortyapi.com/api/character/`
  );
  const [next, setNextPage] = useState("");
  const [previous, setPreviousPage] = useState("");

  useEffect(() => {
    fetch(page)
      .then((response) => response.json())
      .then((response) => {
        setCharacterList(response.results);
        setNextPage(response.info.next);
        setPreviousPage(response.info.prev);
      })
      .catch((err) => console.log(err));
  }, [page]);

  const nextPage = () => {
    if (next !== null) {
      setPage(next);
    }
  };
  const prevPage = () => {
    if (previous !== null) {
      setPage(previous);
    }
  };

  return (
    <div className="App">
      <Characters charactersList={characterList} />
      <div className="buttons">
        <Button text="Voltar" page={previous} func={prevPage} />
        <Button text="Avançar" page={next} func={nextPage} />
      </div>
    </div>
  );
}

export default App;
