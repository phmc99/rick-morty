import { CharCard } from "../CharCard";

import "./style.css";

export const Characters = ({ charactersList }) => {
  return (
    <ul>
      {charactersList.map((item, index) => (
        <li key={index}>
          <CharCard name={item.name} image={item.image} status={item.status} />
        </li>
      ))}
    </ul>
  );
};
