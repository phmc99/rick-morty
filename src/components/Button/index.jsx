import "./style.css";

export const Button = ({ text, page, func }) => {
  return (
    <button onClick={func} disabled={page === null}>
      {text}
    </button>
  );
};
